# sitespeed.io-server

This server will run and wait for *sitespeed.io-clients* to trigger sitespeed.io tests. The results will then be returned to the client.

## Requirements
Node.js needs to be installed.

## Installation
```npm install -g git+https://yesman82@bitbucket.org/yesman82/sitespeed.io-server.git```
You may have to use ```sudo``` powers.

This will install the server on a global level.

## Usage
```sitespeedioserver [-p=33445] [-graphiteHost=false] [-graphitePort=2003]```

Arguments:
- `-p`             Port (optional, default: 33445)
- `-graphiteHost`  Graphite host, e.g. localhost or example.com (optional, default: false)
- `-graphitePort`  Graphite port, e.g. 2003 (optional, default: 2003)

To run the server in background use `nohup`.