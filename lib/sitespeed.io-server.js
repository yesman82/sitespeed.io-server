/**
 * This server listens for TCP socket connections where
 * connected clients can trigger a sitespeed.io test by
 * sending a URL wrapped in a strigified JSON like this:
 *
 *     {
 *       url: 'http://www.example.com'
 *     }
 *
 * The server will then start the test and return the result
 * to the client.
 */

/** @type {Winston} Logging system */
var winston = require('winston');
/** @type {io} socket.io networking library  */
var io = require('socket.io');
/** @type {Sitespeed} sitespeed.io test suite */
var Sitespeed = require('sitespeed.io');
/** @type {Object} Holds graphite server information */
var graphite = {
    /** @type {String} Graphite server hostname */
    host: null,
    /** @type {Number} Graphite server port number */
    port: null
};
/** @type {QueueManager} Manager for queueing tests */
var QueueManager = require('./QueueManager');
/** @type {Xvfb} Xvfb runner */
var Xvfb;
/** @type {caller-id} A utility for getting information on the caller of a function in node.js */
var callerId = require('caller-id');

// windows workaround
try {
    Xvfb = require('xvfb');
} catch (e) {}

// configure logger
logger = new(winston.Logger)({
    transports: [
        new(winston.transports.Console)({
            level: 'info',
            colorize: true
        }),
        new(winston.transports.File)({
            level: 'info',
            json: false,
            label: 'sitespeed-server',
            filename: 'sitespeed.io-server.log'
        })
    ]
});

// changing max listeners to unlimited for production
// https://nodejs.org/api/events.html#events_emitter_setmaxlisteners_n
process.setMaxListeners(0);

/**
 * sitespeed.io server object
 * @param {Number} port TCP port to listen on (default: 33445)
 */
var SitespeedServer = function(port, graphiteHost, graphitePort) {
    port = parseInt(port) || 33445;
    graphite.host = graphiteHost === 'false' ? false : graphiteHost;
    graphitePort = parseInt(graphitePort) || 2003;
    graphite.port = graphitePort > 0 ? graphitePort < 65535 ? graphitePort : 65535 : 0;

    logger.info('Starting Server... Listening on port: ' + port + ' (Graphite server: ' + (graphite.host ? graphite.host + ':' + graphite.port : 'no server specified') + ')');

    this.server.on('connection', this.onSocketConnection.bind(this));
    this.server.on('disconnect', this.onSocketDisconnect);
    this.server.listen(port);

    this.queue = new QueueManager({
        delay: -1
    });
    this.queue.each(this.runTest.bind(this));
};

/** @type {socket.io.Server} Socket.io server */
SitespeedServer.prototype.server = require('socket.io')();

/**
 * Register callbacks on socket when client connects.
 * @param  {socket.io.Socket} socket The socket that just connected
 */
SitespeedServer.prototype.onSocketConnection = function(socket) {
    socket.xvfb = null; // bind xvfb instace to socket
    logger.info(socket.id + ': - CONNECTED -');
    this.sendLog(socket, 'info', 'You are connected as ' + socket.id);

    socket.on('run', this.onSocketDataRun.bind(this, socket));
    socket.on('error', this.onSocketError.bind(this, socket));
};

/**
 * On receiption of data check for a URL and start a sitespeed.io test.
 * @param  {socket.io.Socket} socket The client's socket connection
 * @param  {Object/JSON} options Options to run test with
 */
SitespeedServer.prototype.onSocketDataRun = function(socket, options) {
    /** @type {Object} Holding sitespeed.io options */
    var defaultOptions = {
        xvfb: process.platform === 'linux',
        socket: socket,
        deep: 0,
        browser: 'chrome',
        no: 1,
        graphiteHost: graphite.host,
        graphitePort: graphite.port,
        graphiteData: 'pagemetrics,timings',
        html: false,
        noSandbox: true
    };

    Object.keys(defaultOptions).forEach(function(key) {
        if (!options.hasOwnProperty(key)) {
            options[key] = defaultOptions[key];
        }
    });

    // add test to queue
    logger.info('Queue size: ' + (this.queue.size() + 1)); // log queue size before adding, otherwise timing will be wrong
    this.queue.add(options);

    // inform client about queue position if not first in line
    if (this.queue.indexOf(options) > -1) {
        this.sendLog(socket, 'info', 'There are more tests running. You have been added to the queue.');
        this.sendQueuePosition(socket, this.queue.indexOf(options) + 1);
        options.informInterval = setInterval(function() {
            this.sendQueuePosition(socket, this.queue.indexOf(options) + 1);
        }.bind(this), 15000);
    }
};

/**
 * Log socket errors
 * @param  {Object} error Error information
 */
SitespeedServer.prototype.onSocketError = function(socket, error) {
    logger.error(socket.id + ': ' + error.message);
};

/**
 * Log socket disconnection
 * @param  {socket.io.Socket} socket Client socket
 */
SitespeedServer.prototype.onSocketDisconnect = function(socket) {
    logger.info(socket.id + ': - DISCONNECTED -');
};

/**
 * Run a sitespeed.io test
 * @param  {Object} options sitespeed.io options
 * @param  {socket.io.Socket} socket A socket connection with a client
 */
SitespeedServer.prototype.runTest = function(options) {
    /** @type {Sitespeed} New sitespeed.io test */
    var sitespeed = new Sitespeed();
    /** @type {socket.io.Socket} client socket connection */
    var socket = options.socket;

    delete options.socket; // delete node as sitespeed.io will fail otherwise

    // sending queue position update is no longer needed
    if (options.informInterval) {
        clearInterval(options.informInterval);
        delete options.informInterval;
    }

    // only start test if client still connected
    if (socket.conn.readyState !== 'open') {
        logger.warn(socket.id + ': Client already disconnected.');
        this.nextInQueue();
        return;
    }

    // catching false process exit in browsertime
    process.exit = function(code) {
        logger.info(socket.id + ': Browsertime proxy failed. Disconnecting socket.');
        this.sendLog(socket, 'error', '- TEST FAILED ON SERVER -');
        socket.disconnect();
    }.bind(this);

    this.sendQueuePosition(socket, 0);
    logger.info(socket.id + ': - STARTING TEST -');
    this.sendLog(socket, 'info', '- STARTING TEST -');

    var getSitespeedLogger = function() {
        if (!sitespeed.log) {
            setTimeout(getSitespeedLogger.bind(this), 100);
            return;
        }

        sitespeed.log.on('logging', function(transport, level, msg, meta) {
            if (transport === sitespeed.log.transports.console) {
                this.sendLog(socket, level, msg);
            }
        }.bind(this));
    };

    setTimeout(getSitespeedLogger.bind(this), 100);

    if (!options.xvfb) {
        // run test
        sitespeed.run(options, this.onSitespeedDone.bind(this, socket));
        return;
    }

    // run test in Xvfb buffer
    socket.xvfb = new Xvfb();
    socket.xvfb.start(function(err, xvfbProcess) {
        // code that uses the virtual frame buffer here
        if (err) {
            logger.error(socket.id + ': ' + err.message);
            this.sendLog(socket, 'error', err.message);
            socket.disconnect();
            this.nextInQueue();
            return;
        }

        sitespeed.run(options, this.onSitespeedDone.bind(this, socket));
    }.bind(this));
};

/**
 * Send results to client after sitespeed.io is done.
 * @param  {socket.io.Socket} socket Client connection
 * @param  {Error} error  Error object if something went wrong
 * @param  {Object} data   Result object
 */
SitespeedServer.prototype.onSitespeedDone = function(socket, error, data) {
    /** @type {Object} ySlow results */
    var yslow = null;
    /** @type {Object} chrome timings */
    var timings = null;

    // catching possible property of undefined error
    try {
        yslow = data.result.pages[0].yslow;
    } catch (err) {}

    try {
        timings = data.result.pages[0].timings;
    } catch (err) {}

    // log error or send results
    if (error) {
        logger.error(socket.id + ': ', error.message);
        this.sendLog(socket, 'error', error.message);

    } else {
        logger.info(socket.id + ': sending results');
        // sending shortened results to client
        socket.emit('result', {
            yslow: yslow,
            timings: timings
        });
    }

    // closing connection
    logger.info(socket.id + ': - DONE -');
    this.sendLog(socket, 'info', '- DONE -');

    if (!socket.xvfb) {
        // start next in queue
        socket.disconnect();
        this.nextInQueue();
        return;
    }

    // stop Xvfb if any
    socket.xvfb.stop(function(err) {
        if (err) {
            logger.error(socket.id + ': ' + err.message);
            this.sendLog(socket, 'error', err.message);
        }

        // start next in queue
        socket.disconnect();
        this.nextInQueue();
    }.bind(this));
};

/**
 * Run next test in queue
 */
SitespeedServer.prototype.nextInQueue = function() {
    logger.info('Queue size: ' + this.queue.size());
    this.queue.next();
};


/**
 * Write a winston log message to console and socket.
 * All arguments after the message are metadata and will also be sent.
 * @param  {socket.io.Socket} socket Client connection
 * @param  {String} level   A winston log level
 * @param  {String} message A message to be logged
 */
SitespeedServer.prototype.sendLog = function(socket, level, message) {
    socket.emit('log', {
        level: level,
        message: message
    });
};

/**
 * Send current queue position to client.
 * @param  {socket.io.Socket} socket Client connection
 * @param  {Integer} position Current position in queue
 */
SitespeedServer.prototype.sendQueuePosition = function(socket, position) {
    socket.emit('queuePosition', parseInt(position));
    this.sendLog(socket, 'info', 'You are at position ' + position + '.');
};




module.exports = SitespeedServer;
